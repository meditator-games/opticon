#!/bin/bash

SRCDIR="$(cd "$(dirname "$0")"; pwd -P)"

build() {
  local dir="$SRCDIR/build-$1.$2.$3"
  if [ ! -d "$dir" ]; then
    mkdir -p "$dir"
    cd "$dir"
    local build_type=$([ "$2" == "release" ] && echo "Release" || echo "Debug")
    local toolchain=""
    if [ "$1" == windows ] && [ "$3" == 64 ]; then
      toolchain=mingw-x86_64
    elif [ "$1" == windows ] && [ "$3" == 32 ]; then
      toolchain=mingw-i686
    elif [ "$1" == linux ] && [ "$3" == 32 ]; then
      toolchain=linux-i686
    fi
    local toolchain_arg=""
    if [ -n "$toolchain" ]; then
      toolchain_arg="-DCMAKE_TOOLCHAIN_FILE=../cmake/toolchain-$toolchain.cmake"
    fi
    cmake -G Ninja "-DGODOT_BIN_PATH=$GODOT_BIN_PATH" -DCMAKE_BUILD_TYPE=$build_type "-DGODOT_BITS=$3" "$toolchain_arg" ..
  else
    cd "$dir"
  fi
  ninja
}

if [ ! -d "$GODOT_BIN_PATH" ]; then
  echo "Please set the environment variable GODOT_BIN_PATH to a valid directory
containing the compiled Godot engine libraries." > /dev/stderr
  exit 1
fi

case "$1" in
  linux-release-64)
    build linux release 64
    ;;
  linux-debug-64)
    build linux debug 64
    ;;
  linux-release-32)
    build linux release 32
    ;;
  linux-debug-32)
    build linux debug 32
    ;;
  windows-release-64)
    build windows release 64
    ;;
  windows-debug-64)
    build windows debug 64
    ;;
  windows-release-32)
    build windows release 32
    ;;
  windows-debug-32)
    build windows debug 32
    ;;
  osx-release)
    build osx release 64
    ;;
  osx-debug)
    build osx debug 64
    ;;
  *)
    echo "usage: $0 <type>
  where type is one of:
    linux-release-64
    linux-debug-64
    linux-release-32
    linux-debug-32
    windows-release-64
    windows-debug-64
    windows-release-32
    windows-debug-32
    osx-release
    osx-debug" > /dev/stderr
    exit 1
    ;;
esac
