
.PHONY: help deploy deploy-linux64 deploy-win64 deploy-win32 deploy-macos

help:
	@echo "This makefile can be used to deploy to itch.io. Make sure you export the project"
	@echo "from Godot first."
	@echo
	@echo "  Supported Command            Folder/File"
	@echo "  make deploy-linux64          export/linux64"
	@echo "  make deploy-win64            export/win64"
	@echo "  make deploy-win32            export/win32"
	@echo "  make deploy-macos            export/opticon.macos.zip"
	@echo "  make deploy                  All of the above"

export/opticon.linux64.zip: export/linux64/opticon.x86_64 export/linux64/opticon.pck
	zip -j -u $@ $^

export/opticon.win64.zip: export/win64/opticon.exe export/win64/opticon.pck
	x86_64-w64-mingw32-strip export/win64/opticon.exe
	zip -j -u $@ $^

export/opticon.win32.zip: export/win32/opticon.exe export/win32/opticon.pck
	i686-w64-mingw32-strip export/win32/opticon.exe
	zip -j -u $@ $^

deploy: deploy-linux64 deploy-win64 deploy-win32 deploy-macos

deploy-linux64: export/opticon.linux64.zip
	butler push $^ meditator/opticon:linux-64

deploy-win64: export/opticon.win64.zip
	butler push $^ meditator/opticon:win-64

deploy-win32: export/opticon.win32.zip
	butler push $^ meditator/opticon:win-32

deploy-macos: export/opticon.macos.zip
	butler push $^ meditator/opticon:macos
