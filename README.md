# ![Opticon](https://raw.githubusercontent.com/cyclopsian/opticon/master/project/ui/logo.png)

A procedurally-generated game about giant floating space eyeballs. Made with [Godot Engine](https://godotengine.org). Opticon is [free software](https://www.gnu.org/philosophy/free-sw.en.html). Code is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0-standalone.html). Art and sound is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

To build and modify the project, you'll need to have Godot 3.1. As of this writing it's still in alpha. You can [compile it from source](https://github.com/godotengine/godot), or you can try an [official alpha](https://godotengine.org/article/dev-snapshot-godot-3-1-alpha-2) or [unofficial nightly build](https://hugo.pro/projects/godot-builds/). Open project/project.godot to get started.

Art and music sources are in the 'work' folder. Tools used:

- [Ardour](https://ardour.org)
- [Audacity](https://audacityteam.org)
- [Blender](https://blender.org)
- [GIMP](https://gimp.org)
- [Inkscape](https://inkscape.org)
- [0CC-FamiTracker](http://0cc-famitracker.tumblr.com)
