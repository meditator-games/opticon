/* This file is a placeholder for porting to native! */

#include <core/Godot.hpp>
#include <Reference.hpp>

using namespace godot;

class Dummy : public GodotScript<Reference> {
        GODOT_CLASS(Dummy);
public:
        Dummy() { }
};

/** GDNative Initialize **/
extern "C" void GDN_EXPORT godot_gdnative_init(godot_gdnative_init_options *o)
{
    godot::Godot::gdnative_init(o);
}

/** GDNative Terminate **/
extern "C" void GDN_EXPORT godot_gdnative_terminate(godot_gdnative_terminate_options *o)
{
    godot::Godot::gdnative_terminate(o);
}

/** NativeScript Initialize **/
extern "C" void GDN_EXPORT godot_nativescript_init(void *handle)
{
    godot::Godot::nativescript_init(handle);

    godot::register_class<Dummy>();
}
