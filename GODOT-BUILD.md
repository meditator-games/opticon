```
# tools
scons platform=x11 target=release_debug bits=64 use_static_cpp=yes verbose=yes builtin_libpng=yes LINKFLAGS=--disable-mathve
# X11/POSIX 64 build
scons platform=x11 target=release bits=64 use_static_cpp=yes verbose=yes builtin_libpng=yes tools=no optimize=size disable_advanced_gui=yes module_websocket_enabled=no module_webm_enabled=no module_xatlas_unwrap_enabled=no module_webp_enabled=no module_visual_script_enabled=no use_lto=yes
# Win64 build
scons platform=windows target=release bits=64 use_static_cpp=yes verbose=yes builtin_libpng=yes tools=no optimize=size disable_advanced_gui=yes module_websocket_enabled=no module_webm_enabled=no module_xatlas_unwrap_enabled=no module_webp_enabled=no module_visual_script_enabled=no
```

