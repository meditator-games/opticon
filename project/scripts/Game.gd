# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends Spatial

var water_tick = 0

func _ready():
	Global.cur_player = $Player
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	$Player.global_transform.origin.y = $Terrain.get_heightmap($Player.global_transform.origin).y + 1

func _process(delta):
	resize_reflect_buffer(Settings.reflections)

func resize_reflect_buffer(value):
	if value == 0:
		$WaterReflection.size = Vector2(0, 0)
	else:
		$WaterReflection.size = get_viewport().size / (5 - value)

func _physics_process(delta):
	$Water.transform.origin.x = $Player.transform.origin.x
	$Water.transform.origin.z = $Player.transform.origin.z
	water_tick += delta
	$Water.transform.origin.y = sin(water_tick * 1.5) * 0.5
	$Terrain.material.set_shader_param("water_y", $Water.transform.origin.y)
	$Player.underwater = $Player/Camera.global_transform.origin.y < $Water.global_transform.origin.y
	
	var water_plane = Plane(Vector3(0, 1, 0), $Water.global_transform.origin.y)
	var ct :Transform = $Player/Camera.global_transform
	var vpb = ct.basis
	$WaterReflection/Camera.global_transform = Transform(
		-vpb.x.bounce(water_plane.normal), vpb.y.bounce(water_plane.normal), vpb.z.bounce(water_plane.normal),
		(ct.origin - water_plane.center()).bounce(water_plane.normal) + water_plane.center()).orthonormalized()
#	$Label.text = str(ct) + " : " + str($Viewport/Camera.global_transform)
	#$Label.text = str(ct.basis.determinant()) + " : " + str($Viewport/Camera.global_transform.basis.determinant())