# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends KinematicBody

const Splash = preload("res://actors/particles/Splash.tscn")
#const Bullet = preload("res://Bullet.tscn")
const GRAV = -9.8 * 1.3
#const MAX_SPEED = 5
#const JUMP_SPEED = 2.0
#const ACCEL = 16.0

const MAX_SPEED = 30.0
const JUMP_SPEED = 20.0
const ACCEL = 16.0

const DEACCEL = 8.0
const MAX_X_FOV = 89.9999
const MAX_SLOPE_ANGLE = 40
const MOUSE_SENSITIVITY = 0.05
const DEATH_LINE_Y = -256
const NORMAL_HEIGHT = 0.5
const CROUCH_HEIGHT = 0.01
const CROUCH_OFFSET = NORMAL_HEIGHT - CROUCH_HEIGHT
const CAMERA_HEIGHT = 0.375
const CROUCH_SPEED = 0.15
const PICKUP_REACH = 4.0
const MAX_HP = 100

export (NodePath) var water_path
export (NodePath) var hud_path
onready var water : MeshInstance = get_node(water_path)
onready var hud : Control = get_node(hud_path)

var vel = Vector3()
var spawn_transform
var totally_frozen = false
var frozen = false
var shoot_timer = 0
var death_timer = 0
var camera_y_offset = 0
var crouching = false
var xmove = []
var zmove = []
var since_on_floor = 0
var use_targets = []
var in_vehicle = null
var underwater: bool = false
var last_underwater: bool = false
var hp = MAX_HP
var orbs = 0
#onready var label = Label.new()

export (NodePath) var HighlighterPath = null
onready var _highlighter : Highlighter = get_node(HighlighterPath) if HighlighterPath != null else null

func _ready():
#	add_child(label)
#	label.rect_position = Vector2(64, 64)
	spawn_transform = transform
	
func do_crouch(delta):
	crouching = true
	camera_y_offset = min(camera_y_offset + CROUCH_OFFSET * (1/CROUCH_SPEED) * delta, CROUCH_HEIGHT)
	$CollisionShape2.shape.length = CROUCH_HEIGHT

func _physics_process(delta):
	if underwater != last_underwater and water != null:
		var splash = Splash.instance()
		splash.emitting = true
		splash.restart()
		splash.transform.origin = transform.origin
		splash.transform.origin.y = water.transform.origin.y
		get_parent().add_child(splash)
	last_underwater = underwater
	if totally_frozen:
		return
	if in_vehicle != null:
		return
	var dir = Vector3()
	var cam_xform = $Camera.get_global_transform()
	
	if hp <= 0:
		if death_timer < 1:
			death_timer += delta
		$Camera.rotation_degrees.z = deg2rad(90) * death_timer
		frozen = true
		zmove = []
		xmove = []
		if Input.is_action_just_pressed("click") or Input.is_action_just_pressed("ui_cancel") \
				or Input.is_action_just_pressed("ui_accept") or Input.is_action_just_pressed("ui_select"):
			Global.fade_to(Global, "quit_to_title")

	shoot_timer = max(shoot_timer - delta, 0)
	if not frozen and not Global.is_overlay_modal():
		if Input.is_action_just_pressed("move_forward"):
			zmove.append(-1)
		if Input.is_action_just_released("move_forward"):
			while zmove.has(-1):
				zmove.erase(-1)
		if Input.is_action_just_pressed("move_backward"):
			zmove.append(1)
		if Input.is_action_just_released("move_backward"):
			while zmove.has(1):
				zmove.erase(1)
		if Input.is_action_just_pressed("move_left"):
			xmove.append(-1)
		if Input.is_action_just_released("move_left"):
			while xmove.has(-1):
				xmove.erase(-1)
		if Input.is_action_just_pressed("move_right"):
			xmove.append(1)
		if Input.is_action_just_released("move_right"):
			while xmove.has(1):
				xmove.erase(1)
		if Input.is_action_just_pressed("use") and not use_targets.empty():
			use_targets[0].use(self)
		if Input.is_action_pressed("crouch"):
			do_crouch(delta)
		else:
			crouching = false
		if Input.is_action_just_pressed("shoot") and shoot_timer == 0:
			shoot_timer = 0.25
			#var bullet = Bullet.instance()
			#bullet.transform = $Camera.global_transform
			#get_parent().add_child(bullet)

	if !xmove.empty():
		dir += cam_xform.basis.x.normalized() * xmove[0]
	if !zmove.empty():
		dir += cam_xform.basis.z.normalized() * zmove[0]

	if is_on_floor():
		spawn_transform = transform
		since_on_floor = 0
	else:
		since_on_floor += delta
		
	var snap = Vector3(0, 0, 0) if underwater else Vector3(0, -0.5, 0)
	if (since_on_floor < 0.05 or underwater) and not frozen and not Global.is_overlay_modal():
		if Input.is_action_pressed("jump"):
			if not underwater and not $SoundJump.playing:
				$SoundJump.play()
			snap = Vector3(0, 0, 0)
			vel.y = JUMP_SPEED

	if crouching == false:
		if $CollisionShape2.shape.length == CROUCH_HEIGHT:
			$CollisionShape2.shape.length = NORMAL_HEIGHT
			var collide = move_and_collide(Vector3(0, 1.0/64.0, 0), true, true)
			if collide == null:
				camera_y_offset = max(camera_y_offset - CROUCH_OFFSET * (1/CROUCH_SPEED) * delta, 0)
			else:
				do_crouch(1)
		else:
			camera_y_offset = max(camera_y_offset - CROUCH_OFFSET * (1/CROUCH_SPEED) * delta, 0)

	$Camera.translation.y = CAMERA_HEIGHT - camera_y_offset
	
	dir.y = 0
	dir = dir.normalized()

	var grav = GRAV
	vel.y += delta*grav

	var hvel = vel
	hvel.y = 0

	var target = dir
	target *= MAX_SPEED

	var accel
	if dir.dot(hvel) > 0:
		accel = ACCEL
	else:
		accel = DEACCEL

	hvel = hvel.linear_interpolate(target, accel*delta)
	vel.x = hvel.x
	vel.z = hvel.z
	var last_pos = global_transform.origin
	#vel = move_and_slide_with_snap(vel, snap, Vector3(0, 1, 0), false, true, 3, deg2rad(70))
	var remaining = move_and_slide(vel, Vector3(0, 1, 0), true, 3, deg2rad(60), false)
#	label.text = str(vel)

	if dir.length_squared() > 0 and remaining.length_squared() > 0.25 * 0.25 and is_on_floor():
		if not $SoundStep.playing:
			$SoundStep.play()
	else:
		$SoundStep.stop()

#	if translation.y < DEATH_LINE_Y:
#		transform = spawn_transform
#		translation.y += 8
#		#translation = translation.snapped(Vector3(2, 2, 2)) + Vector3(-1, 2, -1)
#		#print("respawning at " + str(translation))
#		frozen = 1
#		vel = Vector3()
#		xmove = []
#		zmove = []
		
	_highlight_target()
	
func _highlight_target():
	if _highlighter != self and _highlighter != null:
		_highlighter.set_camera($Camera)
		_highlighter.clear_objects()
	
	$Camera/Pickup.force_raycast_update()
	if $Camera/Pickup.is_colliding():
		var obj = $Camera/Pickup.get_collider()
		if _highlighter != self and _highlighter != null:
			_highlighter.add_object(obj)
		if Input.is_action_just_pressed("click") and obj.has_method("use"):
			_aggro()
			obj.use(self)
	if hud != null:
		hud.eat_label.visible = $Camera/Pickup.is_colliding()

func _aggro():
	for enemy in get_tree().get_nodes_in_group("enemies"):
		enemy.begin_chase(self)

func collect(orb):
	_aggro()
	$SoundOrb.play()
	orbs += 1
	orb.queue_free()

func heal(val):
	hp = min(hp + val, MAX_HP)
	$SoundEat.play()
	
func damage(val):
	hp = max(hp - val, 0)
	$SoundDeath.play()

func _unhandled_input(event):
	if totally_frozen or frozen:
		return
	if event is InputEventMouseMotion && Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		$Camera.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY * -1))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))

		if $Camera.rotation_degrees.y != 0:
			$Camera.rotation_degrees = Vector3(MAX_X_FOV * sign(-event.relative.y), 0, 0)
