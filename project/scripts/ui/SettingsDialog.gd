# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends PanelContainer

onready var InputGrid = $VBox/Margins/HBox/Inputs/Scroll/Margins/Grid
onready var InputDummyLabel = $VBox/Margins/HBox/Inputs/Scroll/Margins/Grid/DummyLabel
onready var InputDummyButton = $VBox/Margins/HBox/Inputs/Scroll/Margins/Grid/DummyButton
onready var InputDummyClear = $VBox/Margins/HBox/Inputs/Scroll/Margins/Grid/DummyClear

onready var VideoWindowMode = $VBox/Margins/HBox/VBox/Video/WindowMode
onready var VideoMSAA = $VBox/Margins/HBox/VBox/Video/MSAA
onready var VideoSSAO = $VBox/Margins/HBox/VBox/Video/SSAO
onready var VideoReflections = $VBox/Margins/HBox/VBox/Video/Reflections
onready var AudioEffectsVolume = $VBox/Margins/HBox/VBox/Audio/EffectsVolume
onready var AudioMusicVolume = $VBox/Margins/HBox/VBox/Audio/MusicVolume

class ButtonDelegate:
	var dialog: PanelContainer
	var button: Button
	func _init(d, b):
		dialog = d
		button = b
	func reload_binds():
		var binds = PoolStringArray()
		for input in InputMap.get_action_list(button.get_meta("action")):
			binds.push_back(Global.event_to_text(input))
		button.text = binds.join(", ")
	func bind():
		var bind_dialog = preload("res://ui/BindDialog.tscn").instance()
		button.get_tree().root.add_child(bind_dialog)
		var action = button.get_meta("action")
		bind_dialog.bind(action, Global.format_action_name(action))
		yield(bind_dialog, "hide")
		bind_dialog.visible = false
		bind_dialog.queue_free()
		dialog.reload_all_binds()
		Settings.save()
	func clear():
		InputMap.action_erase_events(button.get_meta("action"))
		reload_binds()

var button_delegates = []

func reload_all_binds():
	for delegate in button_delegates:
		delegate.reload_binds()

func _ready():
	for action in InputMap.get_actions():
		if not action.begins_with("ui_") and action != "click":
			var label = InputDummyLabel.duplicate()
			label.text = Global.format_action_name(action)
			
			var button = InputDummyButton.duplicate()
			var delegate = ButtonDelegate.new(self, button)
			button.set_meta("action", action)
			delegate.reload_binds()
			button.connect("pressed", delegate, "bind")
			
			var clear = InputDummyClear.duplicate()
			clear.connect("pressed", delegate, "clear")
			
			button_delegates.append(delegate)
			
			InputGrid.add_child(label)
			InputGrid.add_child(button)
			InputGrid.add_child(clear)
	
	InputDummyLabel.free()
	InputDummyButton.free()
	InputDummyClear.free()
	
	VideoWindowMode.add_item("Windowed", 0)
	VideoWindowMode.add_item("Fullscreen", 1)
	VideoWindowMode.add_item("Fullscreen Borderless", 2)
	VideoWindowMode.select(Settings.window_mode)
	
	VideoMSAA.add_item("Disabled", Viewport.MSAA_DISABLED)
	VideoMSAA.add_item("2x", Viewport.MSAA_2X)
	VideoMSAA.add_item("4x", Viewport.MSAA_4X)
	VideoMSAA.add_item("8x", Viewport.MSAA_8X)
	VideoMSAA.add_item("16x", Viewport.MSAA_16X)
	VideoMSAA.select(Settings.msaa)
	
	VideoSSAO.add_item("Disabled", 0)
	VideoSSAO.add_item("Low", 1)
	VideoSSAO.add_item("Medium", 2)
	VideoSSAO.add_item("High", 3)
	VideoSSAO.select(Settings.ssao)
	
	VideoReflections.add_item("Disabled", 0)
	VideoReflections.add_item("Low", 1)
	VideoReflections.add_item("Medium", 2)
	VideoReflections.add_item("High", 3)
	VideoReflections.add_item("Ultra High", 4)
	VideoReflections.select(Settings.reflections)
	
	AudioEffectsVolume.value = Settings.sfx_volume
	AudioMusicVolume.value = Settings.music_volume

func _on_WindowMode_item_selected(id):
	Settings.window_mode = id
	Settings.save()

func _on_MSAA_item_selected(id):
	Settings.msaa = id
	Settings.save()
	
func _on_SSAO_item_selected(id):
	Settings.ssao = id
	Settings.save()

func _on_Reflections_item_selected(id):
	Settings.reflections = id
	Settings.save()

func _on_EffectsVolume_value_changed(value):
	Settings.sfx_volume = value
	Settings.save()

func _on_MusicVolume_value_changed(value):
	Settings.music_volume = value
	Settings.save()

func _on_ResetBindsButton_pressed():
	Settings.reset_binds()
	Settings.save()
	reload_all_binds()
