# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends ViewportContainer

class_name Highlighter

onready var BaseVP = $Viewport/BlurY/Viewport/BlurX/Viewport/Base/Viewport

class HighlightMaterial extends SpatialMaterial:
	func _init():
		._init()
		flags_unshaded = true
		
class WeightTexture extends ImageTexture:
	func _init(ksize: int, sigma: float):
		._init()
		var sd = sigma * sigma * -2.0
		var weight_img = Image.new()
		weight_img.create(ksize, 1, false, Image.FORMAT_RF)
		
		var weights = PoolRealArray()
		var sum = 0
		for i in range(1, ksize + 1):
			var w = exp((i * i) / sd)
			weights.push_back(w)
			sum += w
		weight_img.lock()
		for i in range(weights.size()):
			var c = weights[i] / (sum * 2.0)
			weight_img.set_pixel(i, 0, Color(c, c, c, 1))
		weight_img.unlock()
			
		create_from_image(weight_img, 0)

onready var highlight_material = HighlightMaterial.new()
onready var weight_tex = WeightTexture.new(13, 8)

var _camera : Camera = null
var _highlights = {}

func _ready():
	material.set_shader_param("mask_texture", BaseVP.get_texture())
	$Viewport/BlurY.material.set_shader_param("blur_weights", weight_tex)
	$Viewport/BlurY/Viewport/BlurX.material.set_shader_param("blur_weights", weight_tex)

func _set_hilight_mat(obj: Node, mat: Material):
	if obj is MeshInstance:
		obj.material_override = mat
	for c in obj.get_children():
		_set_hilight_mat(c, mat)

func _add_highlight(obj: Node):
	visible = true
	var hi_obj = obj.duplicate(0)
	hi_obj.transform = obj.global_transform
	_set_hilight_mat(hi_obj, highlight_material)
	BaseVP.add_child(hi_obj)
	_highlights[obj.get_instance_id()] = hi_obj

func _free_camera():
	if _camera != null:
		_camera.free()
		_camera = null

func set_camera(cam: Camera):
	_free_camera()
	if cam != null:
		_camera = cam.duplicate(0)
		_camera.transform = cam.global_transform
		BaseVP.add_child(_camera)
	
func add_object(obj: Node, cam: Camera = null):
	if cam != null:
		set_camera(cam)
	_add_highlight(obj)

func remove_object(obj: Node):
	var id = obj.get_instance_id()
	if _highlights.has(id):
		_highlights[id].free()
		_highlights.erase(id)
	
func clear_objects():
	visible = false
	for obj in _highlights.values():
		obj.free()
	_highlights.clear()
