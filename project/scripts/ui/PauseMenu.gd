# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends ColorRect

export (NodePath) var hud_path
onready var hud : Control = get_node(hud_path)
export (NodePath) var player_path
onready var player : Spatial = get_node(player_path)
onready var PauseMenu = $PauseViewport/Viewport/Overlay/PauseMenu
onready var current_panel = PauseMenu

func _ready():
	visible = false
	modulate = Color(1, 1, 1, 0)
	_toggle_menu(true)

func _fade_to(next_panel: Control):
	if $Fader.is_active():
		return
	$Fader.interpolate_property($PauseViewport, "modulate", null, Color(1.0, 1.0, 1.0, 0.0), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fader.start()
	var old_panel = current_panel
	current_panel = next_panel
	yield($Fader, "tween_completed")
	$Fader.remove_all()
	old_panel.visible = false
	next_panel.visible = true
	$Fader.interpolate_property($PauseViewport, "modulate", null, Color(1.0, 1.0, 1.0, 1.0), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fader.start()
	yield($Fader, "tween_completed")
	$Fader.remove_all()
	
func _toggle_menu(enabled):
	if current_panel != PauseMenu:
		_fade_to(PauseMenu)
		return
		
	get_tree().paused = not enabled
	if enabled:
		mouse_filter = Control.MOUSE_FILTER_IGNORE
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		$OverlayFader.interpolate_property(self, "modulate", Color(1, 1, 1, 1), Color(1, 1, 1, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$OverlayFader.start()
		yield($OverlayFader, "tween_completed")
		$OverlayFader.remove_all()
		visible = false
		hud.visible = true
		$PauseViewport/Viewport.gui_disable_input = true
	else:
		current_panel.visible = false
		current_panel = PauseMenu
		current_panel.visible = true
		visible = true
		hud.visible = false
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		$OverlayFader.interpolate_property(self, "modulate", Color(1, 1, 1, 0), Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$OverlayFader.start()
		yield($OverlayFader, "tween_completed")
		$OverlayFader.remove_all()
		mouse_filter = Control.MOUSE_FILTER_STOP
		$PauseViewport/Viewport.gui_disable_input = false

func _process(delta):
	if not Global.is_overlay_modal() and Input.is_action_just_pressed("ui_cancel"):
		if get_tree().paused or player.hp > 0:
			_toggle_menu(get_tree().paused)

func _on_ResumeButton_pressed():
	_toggle_menu(true)

func _on_QuitButton_pressed():
	Global.fade_to(Global, "quit")

func _on_Settings_BackButton_pressed():
	_fade_to(PauseMenu)

func _on_SettingsButton_pressed():
	_fade_to($PauseViewport/Viewport/SettingsDialog)

func _on_TitleButton_pressed():
	Global.fade_to(Global, "quit_to_title")