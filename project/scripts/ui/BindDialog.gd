# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends ColorRect

var _current_action

func bind(action: String, name: String):
	_current_action = action
	$BindDialog/Margins/VBox/BindLabel.text = "Press Key for %s..." % name
	show_modal(true)
	$BindDialog.grab_focus()

func _input(event: InputEvent):
	if event is InputEventMouseMotion:
		get_tree().set_input_as_handled()
		return
	if event is InputEventMouseButton:
		get_tree().set_input_as_handled()
		return
	if event.get("pressed") == false:
		return
	if not (event is InputEventKey) or not event.pressed or event.scancode != KEY_ESCAPE:
		for action in InputMap.get_actions():
			InputMap.action_erase_event(action, event)
		InputMap.action_add_event(_current_action, event)
	visible = false