# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends PanelContainer

func _ready():
	$VBox/Margins/Scroller/VBox/CodeLicense.text = _file_read("res://licenses/GPL-3.0.txt")
	$VBox/Margins/Scroller/VBox/ArtLicense.text = _file_read("res://licenses/CC-BY-SA-4.0.txt")
	$VBox/Margins/Scroller/VBox/EngineLicense.text = _file_read("res://licenses/GODOT-MIT.txt")
	$VBox/Margins/Scroller/VBox/ThirdPartyLicense.text = _file_read("res://licenses/GODOT-THIRDPARTY.txt")
	$VBox/Margins/Scroller/VBox/FontLicense.text = _file_read("res://licenses/Baloo-OFL.txt")

func _file_read(filename):
	var file = File.new()
	file.open(filename, File.READ)
	var text = file.get_as_text()
	file.close()
	return text

func _on_CodeLicenseButton_toggled(button_pressed):
	$VBox/Margins/Scroller/VBox/CodeLicense.visible = button_pressed

func _on_ArtLicenseButton_toggled(button_pressed):
	$VBox/Margins/Scroller/VBox/ArtLicense.visible = button_pressed


func _on_EngineLicenseButton_toggled(button_pressed):
	$VBox/Margins/Scroller/VBox/EngineLicense.visible = button_pressed

func _on_FontLicenseButton_toggled(button_pressed):
	$VBox/Margins/Scroller/VBox/FontLicense.visible = button_pressed

func _on_ThirdPartyLicenseButton_toggled(button_pressed):
	$VBox/Margins/Scroller/VBox/ThirdPartyLicense.visible = button_pressed