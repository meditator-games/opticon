# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends Node

onready var _panel_stack = [$MenuViewport/Viewport/Title]
onready var NewSeed = $MenuViewport/Viewport/NewContainer/NewDialog/Margins/ParamVBox/Params/Seed
var _loading
var _loading_complete = false
var _initial_game_load = true

func _ready():
	randomize()
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	_panel_stack[0].visible = true
	$MenuViewport/Viewport/Title/Buttons/NewButton.grab_focus()

func _set_new_game_loading(on):
	$MenuViewport/Viewport/NewContainer/NewDialog/Margins/ParamVBox.visible = not on
	$MenuViewport/Viewport/NewContainer/NewDialog/Margins/ProgressVBox.visible = on
	
func _is_new_game_loading():
	return $MenuViewport/Viewport/NewContainer/NewDialog/Margins/ProgressVBox.visible
	
func _set_game_load_progress(val: float):
	$MenuViewport/Viewport/NewContainer/NewDialog/Margins/ProgressVBox/GenerateProgress.value = val

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
#		if _loading != null:
#			_loading.free()
#			_loading = null
#			_set_new_game_loading(false)
		if _loading == null and _panel_stack.size() > 1 and not _loading_complete:
			_pop_menu()
	
	if _is_new_game_loading():
		if _loading_complete:
			_set_game_load_progress(1.0)
		elif _loading == null:
			_set_game_load_progress(Global.game_load_progress / 4.0)
			if Global.Game != null:
				_loading = Global.Game.instance()
				_loading.start_load(NewSeed.value)
		else:
			_loading.tick_load()
			if _initial_game_load:
				_set_game_load_progress((Global.game_load_progress + _loading.load_progress * 3.0) / 4.0)
			else:
				_set_game_load_progress(_loading.load_progress)
			if _loading.load_progress >= 1:
				Global.fade_to(Global, "goto_scene", [_loading])
				_loading = null
				_loading_complete = true

func _fade(from: Control, to: Control):
	$Fader.interpolate_property($MenuViewport, "modulate", null, Color(1.0, 1.0, 1.0, 0.0), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fader.start()
	yield($Fader, "tween_completed")
	$Fader.remove_all()
	from.visible = false
	to.visible = true
	$Fader.interpolate_property($MenuViewport, "modulate", null, Color(1.0, 1.0, 1.0, 1.0), 0.4, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Fader.start()
	yield($Fader, "tween_completed")
	$Fader.remove_all()
	
func _push_menu(panel: Control):
	if not $Fader.is_active():
		_panel_stack.push_back(panel)
		_fade(_panel_stack[-2], _panel_stack[-1])

func _pop_menu():
	if not $Fader.is_active():
		var last = _panel_stack.pop_back()	
		_fade(last, _panel_stack[-1])

func _on_NewButton_pressed():
	_set_new_game_loading(false)
	NewSeed.value = randi() % int(NewSeed.max_value)
	_push_menu($MenuViewport/Viewport/NewContainer)

func _on_SettingsButton_pressed():
	_push_menu($MenuViewport/Viewport/SettingsDialog)

func _on_CreditsButton_pressed():
	_push_menu($MenuViewport/Viewport/Credits)

func _on_QuitButton_pressed():
	Global.fade_to(Global, "quit")

func _on_Settings_BackButton_pressed():
	_pop_menu()

func _on_Credits_BackButton_pressed():
	_pop_menu()
	
func _on_NewGame_BackButton_pressed():
	_pop_menu()

func _on_GenerateButton_pressed():
	_set_new_game_loading(true)
	var Game = Global.start_new_game()
	if Game != null:
		_initial_game_load = false
		_loading = Game.instance()
		_loading.start_load(NewSeed.value)


