# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends RigidBody

var BLINK_DURATION = 0.2

enum BlendShapes { CLOSED, ANGRY }
enum EyeState { IDLE, LOOK, CHASE, ATTACK, DEAD }
var state : int = EyeState.IDLE
var blend_values = [0, 0]
var blend_targets = [0, 0]
var target: Spatial = null
var tired = 0.2
var anger = 0
var terrain

var attack_cooldown_timer = 10
var move_timer = 0
var rotate_timer = 0
var rotate_target
var blink_timer = -2
var blink_step = 0
var attack_timer = 0
var laser_colliding = []
onready var home = global_transform.origin

const CHARGE_TIME = 1.0
const FIRE_TIME = 1.0
const END_TIME = 0.2
const ROTATE_SPEED = PI / 8.0
const IDLE_MOVE_SPEED = 32.0
const CHASE_SPEED = 20.0
const FLOAT_HEIGHT = 128.0
const SIGHT_DISTANCE = 192.0
const CHASE_DISTANCE = 224.0
const HOME_DISTANCE = 128.0

onready var Laser = $EyeBody/Eyeball/Laser
onready var LaserBeam = $EyeBody/Eyeball/Laser/Beam
onready var LaserArea = $EyeBody/Eyeball/Laser/Area
onready var LaserAreaShape = $EyeBody/Eyeball/Laser/Area/CollisionShape
onready var LaserRay = $EyeBody/Eyeball/RayCast

static func ease_quad_inout(t):
	t *= 2
	return (t * t if t <= 1 else (t - 1) * (2 - (t - 1)) + 1) / 2.0

func _ready():
	var eyeball = $EyeBody/Outer
	eyeball.mesh = eyeball.mesh.duplicate()
	eyeball.set_surface_material(0, eyeball.mesh.surface_get_material(0).duplicate())
	var outer = $EyeBody/Outer
	outer.mesh = outer.mesh.duplicate()
	LaserBeam.mesh = LaserBeam.mesh.duplicate()
	LaserBeam.set_surface_material(0, preload("res://materials/Laser.material"))
	LaserAreaShape.shape = LaserAreaShape.shape.duplicate()
		
func _physics_process(delta):
	if not is_inside_tree():
		return
	$EyeBody/Outer.rotation_degrees.y = 0
	$EyeBody/Eyeball/Eyeball.mesh.surface_get_material(0).emission_energy = 0.0
	$EyeBody/Eyeball/StartLight.light_energy = 0.0
	if target != null and target.hp == 0:
		target = null
	match state:
		EyeState.IDLE:
			$EyeBody/Outer.rotation_degrees.x = tired * 25.0
			var step = ease_quad_inout(1 - abs(blink_step * 2.0 / BLINK_DURATION - 1))
			blend_targets[BlendShapes.CLOSED] = step * (1.0 - tired) + tired
			blend_targets[BlendShapes.ANGRY] = anger * (1.0 - step)
			if blink_step > 0:
				blink_step = max(blink_step - delta, 0)
			blink_timer += delta
			if blink_timer > randf() * 64.0 + BLINK_DURATION * 2.0:
				blink_step = BLINK_DURATION
				blink_timer = -blink_step
				
			if move_timer > 0:
				move_timer -= delta
				_move_forward(delta)
			elif randf() > 0.996:
				move_timer = rand_range(1.0, 3.0)
				
			if rotate_timer > 0:
				rotate_timer -= delta
				_look_towards(transform.origin + rotate_target, delta)
			elif randf() > 0.996:
				rotate_timer = rand_range(0.5, 2.0)
				rotate_target = transform.basis.rotated(Vector3(0, 1, 0), rand_range(deg2rad(30), deg2rad(160)) * (-1 if randi() % 2 else 1)).z
			else:
				_look_towards(transform.origin + transform.basis.z * Vector3(1, 0, 1), delta)
				
			_try_acquire_target(SIGHT_DISTANCE)
				
		EyeState.LOOK:
			$EyeBody/Outer.rotation_degrees.x = 0.0
			blend_targets[BlendShapes.CLOSED] = 0
			blend_targets[BlendShapes.ANGRY] = anger
			_try_acquire_target(SIGHT_DISTANCE)
			if target != null:
				_look_towards(target.global_transform.origin, delta)
		EyeState.CHASE:
			if target == null:
				_try_acquire_target(SIGHT_DISTANCE)
				if target == null:
					state = EyeState.IDLE
			else:
				_try_acquire_target(CHASE_DISTANCE)
				if target != null:
					_look_towards(target.global_transform.origin, delta)
					_move_forward(delta)
					if attack_cooldown_timer > 0:
						attack_cooldown_timer -= delta
						if randi() % 50 == 0:
							_begin_attack()
					
		EyeState.ATTACK:
			$EyeBody/Outer.rotation_degrees.x = 0.0
			blend_targets[BlendShapes.CLOSED] = 0
			blend_targets[BlendShapes.ANGRY] = ease_quad_inout(clamp(attack_timer / 0.2, 0.0, 1.0))
			var charge_step = clamp(attack_timer / CHARGE_TIME, 0.0, 1.0)
			$EyeBody/Eyeball/Eyeball.mesh.surface_get_material(0).emission_energy = charge_step * 3.0
			$EyeBody/Eyeball/StartLight.light_energy = charge_step * 16.0
			$EyeBody/Outer.rotation_degrees.y = 4.0 * rand_range(-1.0, 1.0) * charge_step
			if attack_timer >= CHARGE_TIME + FIRE_TIME + END_TIME:
				$EyeBody/Eyeball/Eyeball.mesh.surface_get_material(0).emission_energy = 0
				$EyeBody/Eyeball/StartLight.light_energy = 0.0
				attack_timer = 0
				attack_cooldown_timer = 10
				state = EyeState.CHASE
				anger = 1.0
			if attack_timer >= CHARGE_TIME + FIRE_TIME:
				LaserRay.enabled = false
				Laser.visible = false
				LaserArea.monitoring = false
				if $SoundLaser.playing:
					$SoundLaser.stop()
				var uncharge_step = clamp(1.0 - (attack_timer - (CHARGE_TIME + FIRE_TIME)) / END_TIME, 0.0, 1.0)
				$EyeBody/Eyeball/Eyeball.mesh.surface_get_material(0).emission_energy = uncharge_step * 3.0
				$EyeBody/Eyeball/StartLight.light_energy = uncharge_step * 16.0
			elif attack_timer >= CHARGE_TIME:
				if not Laser.visible:
					$SoundLaser.play()
				if target != null:
					_look_towards(target.global_transform.origin, delta / 16.0)
				Laser.visible = true
				LaserArea.monitoring = true
				LaserRay.force_raycast_update()
				var laser_target = $EyeBody/Eyeball.global_transform.origin + $EyeBody/Eyeball.global_transform.basis.z * -2048.0
				if LaserRay.is_colliding():
					laser_target = LaserRay.get_collision_point() + $EyeBody/Eyeball.global_transform.basis.z
				var laser_ray = $EyeBody/Eyeball.global_transform.origin - laser_target
				LaserBeam.mesh.mid_height = laser_ray.length()
				#LaserBeam.mesh.rings = LaserBeam.mesh.mid_height / 32
				LaserBeam.transform.origin.y = LaserBeam.mesh.mid_height / 2.0
				LaserAreaShape.shape.height = LaserBeam.mesh.mid_height
				LaserArea.transform.origin.y = LaserBeam.transform.origin.y
				$EyeBody/Eyeball/Laser/EndLight.transform.origin.y = LaserBeam.transform.origin.y
				for collider in laser_colliding:
					_do_damage(collider)
			elif target != null:
				_look_towards(target.global_transform.origin, delta)
			attack_timer += delta
		
				
	for i in range(blend_values.size()):
		blend_values[i] = blend_targets[i]
		VisualServer.instance_set_blend_shape_weight($EyeBody/Outer._get_visual_instance_rid(), i, blend_values[i])

func _look_towards(pos: Vector3, delta: float):
	var current_rot = Quat(transform.basis)
	var target_basis = transform.looking_at(pos, Vector3(0, 1, 0)).basis
	target_basis = target_basis.rotated(target_basis.y, deg2rad(180))
	var target_rot = Quat(target_basis)
	var q = current_rot.inverse() * target_rot
	var a = abs(atan2(Vector3(q.x, q.y, q.z).length(), q.w))
	if a > ROTATE_SPEED * delta:
		transform.basis = Basis(current_rot.slerp(target_rot, ROTATE_SPEED * delta / a))
	else:
		transform.basis = target_basis

func _try_acquire_target(dist: float):
	var dist_squared = dist * dist
	if target != null and global_transform.origin.distance_squared_to(target.global_transform.origin) > dist_squared:
		target = null
	if target == null:
		var potential_targets = get_tree().get_nodes_in_group("players")
		potential_targets.shuffle()
		for player in potential_targets:
			if global_transform.origin.distance_squared_to(player.global_transform.origin) <= dist_squared:
				target = player
				state = EyeState.LOOK
				break
	if target == null:
		state = EyeState.IDLE

func begin_chase(t: Spatial):
	if target == null or target == t:
		var dist_squared = SIGHT_DISTANCE * SIGHT_DISTANCE
		if global_transform.origin.distance_squared_to(t.global_transform.origin) <= dist_squared:
			var vp = get_viewport()
			var space = vp.world.get_direct_space_state()
			var result = space.intersect_ray(global_transform.origin, t.global_transform.origin, [], LaserArea.collision_mask ^ 1)
			if not result.has("collider"):
				state = EyeState.CHASE
				target = t

func _move_forward(delta):
	var new_pos = transform.origin + transform.basis.z * CHASE_SPEED * delta
	new_pos.y = terrain.get_heightmap(new_pos).y + FLOAT_HEIGHT
	if new_pos.distance_squared_to(home) < HOME_DISTANCE * HOME_DISTANCE:
		transform.origin = new_pos
		
func _begin_attack():
	state = EyeState.ATTACK
	attack_timer = 0
	rotate_timer = 0
	move_timer = 0

func _do_damage(t):
	t.damage(rand_range(1, 2))

func _on_Laser_Area_body_entered(body):
	if body.has_method("damage"):
		laser_colliding.append(body)
		_do_damage(body)

func _on_Laser_Area_body_exited(body):
	laser_colliding.erase(body)
