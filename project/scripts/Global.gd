extends Node

const DEFAULT_FADE_DURATION = 0.5

var Game: PackedScene

# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

var _game_loader_thread: Thread
var game_load_progress: float = 0

var fade_cb = null
var fade_overlay = null
var last_overlay_alpha = 1
var current_scene = null
var fade_duration = DEFAULT_FADE_DURATION
var cur_player: Spatial = null
var overlay_stack = []

func _ready():
	_start_fade(false)

func _game_loader_loop(obj):
	var loader = ResourceLoader.load_interactive("res://scenes/Game.tscn")
	
	while loader.poll() == OK:
		call_deferred("_set_load_progress", loader.get_stage() / float(loader.get_stage_count()))
	call_deferred("_game_loaded", loader.get_resource())

func _set_load_progress(val: float):
	game_load_progress = val

func _game_loaded(game: PackedScene):
	game_load_progress = 1.0
	Game = game

func _enter_tree():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() -1)
	get_tree().current_scene = current_scene
	_recreate_fade_overlay()

func _start_fade(on):
	var color = Color(1, 1, 1, 0)
	if on:
		color.a = 1
	$Tween.stop_all()
	$Tween.remove_all()
	$Tween.interpolate_property(fade_overlay, "modulate", null, color, fade_duration, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func _process(delta):
	last_overlay_alpha = fade_overlay.modulate.a
	if not $Tween.is_active():
		if fade_cb != null:
			fade_cb[0].callv(fade_cb[1], fade_cb[2])
			fade_cb = null
		elif fade_overlay.modulate.a > 0:
			_start_fade(false)
	if $Tween.tell() == 0:
		fade_overlay.visible = fade_overlay.modulate.a > 0

func _recreate_fade_overlay():
	fade_overlay = preload("res://ui/FadeOverlay.tscn").instance()
	fade_overlay.modulate.a = last_overlay_alpha
	fade_overlay.name = "FadeOverlay"
	get_tree().root.get_children().back().add_child(fade_overlay)
	
func goto_scene(scene: Node):
	current_scene.free()
	current_scene = scene
	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene
	_recreate_fade_overlay()

func fade_to(obj, method, args=[]):
	fade_cb = [obj, method, args]
	fade_overlay.visible = true
	_start_fade(true)
	
func push_overlay(control, modal=false):
	if not overlay_stack.empty():
		overlay_stack[-1]["control"].visible = false
	overlay_stack.append({"control": control, "modal": modal})
	get_tree().root.add_child(control)
			
func remove_overlay(control):
	for entry in overlay_stack:
		if entry["control"] == control:
			overlay_stack.erase(entry)
			control.queue_free()
			break
	if not overlay_stack.empty():
		overlay_stack[-1]["control"].visible = true

func is_overlay_modal():
	if not overlay_stack.empty():
		return overlay_stack[-1]["modal"]
	return false

func current_overlay():
	if not overlay_stack.empty():
		return overlay_stack[-1]["control"]
	return null

func format_action_name(action: String):
	var words = action.split("_")
	for i in range(len(words)):
		words[i] = words[i][0].to_upper() + words[i].substr(1, words[i].length() - 1)
	return words.join(" ")

func event_to_text(event: InputEvent):
	match event.get_class():
		"InputEventJoypadMotion":
			return "Pad " + ["X","Y"][event.axis % 2] + str(event.axis / 2) + ("-" if event.axis_value == -1 else "+")
		"InputEventJoypadButton":
			return "Pad B" + str(event.button_index)
		_:
			return event.as_text()
			
func get_bind_str(action):
	var keys = PoolStringArray()
	for key in InputMap.get_action_list(action):
		keys.push_back(event_to_text(key))
	return keys.join(" or ")
	
func start_new_game():
	fade_duration = DEFAULT_FADE_DURATION
	if _game_loader_thread == null:
		_game_loader_thread = Thread.new()
		_game_loader_thread.start(self, "_game_loader_loop")
	return Game
	
func quit_to_title():
	fade_duration = DEFAULT_FADE_DURATION
	get_tree().paused = false
	goto_scene(preload("res://ui/Title.tscn").instance())
	
func quit():
	get_tree().quit()
	
func get_game_viewport():
	if current_scene != null and current_scene.has_node("Game/Viewport"):
		return current_scene.get_node("Game/Viewport")
	return null

