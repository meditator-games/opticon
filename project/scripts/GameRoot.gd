# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends Control

var load_progress: float = 0

func start_load(terrain_seed):
	$Game/Viewport/Root/Terrain.random.seed = terrain_seed
	$Game/Viewport/Root/Terrain.start_work_thread()
	
func tick_load():
	$Game/Viewport/Root/Terrain.process_worldgen()
	load_progress = $Game/Viewport/Root/Terrain.load_progress

func _process(delta):
	$HUD.hp_bar.value = $Game/Viewport/Root/Player.hp
	$HUD.orb_count.text = str($Game/Viewport/Root/Player.orbs)