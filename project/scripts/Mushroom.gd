# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends RigidBody

export (bool) var edible = false
export (Material) var material = null

var chunk

var rad = 0.5
var stem_rad = rad * 0.1
var stem_rad_top = rad * 0.08
var height = 1.0
var cap_top_yscale = 0.4
var cap_bot_yscale = 0.4
var base_type = 'rounded'

var stem_base_rad = rad * 0.2
var stem_base_yscale = 0.6

var cap_bot_steps = 2.0
var cap_top_steps = 4.0
var base_steps = 2.0
var lathe_steps = 8.0
	
const HR2 = sin(PI * 0.25)
const R2 = HR2 * 2.0

const EDIBLE_VIEW_DIST = pow(512.0, 2)

func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		if chunk != null:
			chunk.objects.erase(self)

func _ready():
	regenerate()

func use(user):
	if user.has_method("heal"):
		user.heal(rand_range(4, 6))
	queue_free()

func damage(val):
	queue_free()

func _physics_process(delta):
	if not is_inside_tree():
		return
	if not edible:
		return
	var vp = get_viewport()
	if vp == null:
		return
	var camera = vp.get_camera()
	if camera == null:
		return
	visible = global_transform.origin.distance_squared_to(camera.global_transform.origin) < EDIBLE_VIEW_DIST

func regenerate():
	var stem_height = height - cap_top_yscale * rad + cap_bot_yscale * rad * (R2 - 1.0)
	
	var lathe = []
	if base_type == 'rounded':
		for i in range(base_steps * 2 + 1):
			var ca = i / base_steps * PI * 0.5
			if ca > PI * 0.75:
				var el = Vector2(stem_rad, stem_base_rad * stem_base_yscale) # bulb bottom ellipse
				var er = Vector2(stem_base_rad - stem_rad, stem_base_rad * stem_base_yscale) # bulb top first ellipse, same direction but scaled in x
				var erb = er * (1 + R2) # bulb top 2nd ellipse, curves out back to stem (silver ratio)
				var oc = el + er * HR2 + erb * HR2
				lathe.append(oc + Vector2(cos(ca) * erb.x, -sin(ca) * erb.y))
			elif ca > PI * 0.5:
				lathe.append(Vector2(stem_rad + sin(ca) * (stem_base_rad - stem_rad), (1.0 - cos(ca)) * stem_base_rad * stem_base_yscale))
			else:
				lathe.append(Vector2(sin(ca), (1.0 - cos(ca)) * stem_base_yscale) * stem_base_rad)
			if lathe[-1].y >= stem_height:
				lathe[-1].y = stem_height
				break
		if lathe[-1].y < stem_height:
			lathe.append(Vector2(stem_rad_top, stem_height))
	else:
		lathe.append(Vector2(0, 0))
		lathe.append(Vector2(stem_rad, 0))
		lathe.append(Vector2(stem_rad_top, stem_height))
	var bs = Vector2(rad - stem_rad_top, rad * cap_bot_yscale) # cap bot curve size
	var bc = Vector2(stem_rad_top, stem_height) + bs # cap bot curve center
	var ts = Vector2(rad, rad * cap_top_yscale) # cap top curve size
	var tc = Vector2(0, height - ts.y) # cap top curve center
	for i in range(cap_bot_steps + 1):
		var ba = (i + cap_bot_steps) / (cap_bot_steps * 2.0) * PI * 0.5 + PI * 0.5
		var of = Vector2(-sin(ba), cos(ba)) * bs * R2
		lathe.append(bc + of)
	for i in range(cap_top_steps + 1.0):
		var ta = i / cap_top_steps * PI * 0.5
		var of = Vector2(cos(ta), sin(ta)) * ts
		lathe.append(tc + of)
	
	var normals_2d = [Vector2(0, -1), Vector2(0, 1)]
	for i in range(1, lathe.size() - 1):
		normals_2d.insert(i, ((lathe[i] - lathe[i - 1]).tangent() - (lathe[i] - lathe[i + 1]).tangent()).normalized())
	
	var inst = MeshInstance.new()
	var surface = SurfaceTool.new()
	surface.begin(Mesh.PRIMITIVE_TRIANGLES)
	for vi in range(lathe.size() - 1):
		var va = lathe[vi]
		var vna = normals_2d[vi]
		var vb = lathe[vi + 1]
		var vnb = normals_2d[vi + 1]
		for i in range(lathe_steps):
			var ar = i / lathe_steps * PI * 2.0
			var br = (i + 1) / lathe_steps * PI * 2.0
			var a = Vector3(va.x * cos(ar), va.y, va.x * sin(ar))
			var b = Vector3(va.x * cos(br), va.y, va.x * sin(br))
			var c = Vector3(vb.x * cos(ar), vb.y, vb.x * sin(ar))
			var d = Vector3(vb.x * cos(br), vb.y, vb.x * sin(br))
			var na = Vector3(vna.x * cos(ar), vna.y, vna.x * sin(ar))
			var nb = Vector3(vna.x * cos(br), vna.y, vna.x * sin(br))
			var nc = Vector3(vnb.x * cos(ar), vnb.y, vnb.x * sin(ar))
			var nd = Vector3(vnb.x * cos(br), vnb.y, vnb.x * sin(br))
			var uva = Vector2(i / lathe_steps, vi / lathe.size())
			var uvb = Vector2((i + 1) / lathe_steps, vi / lathe.size())
			var uvc = Vector2(i / lathe_steps, (vi + 1) / lathe.size())
			var uvd = Vector2((i + 1) / lathe_steps, (vi + 1) / lathe.size())
			
			if b != a:
				surface.add_uv(uvc)
				surface.add_normal(nc)
				surface.add_vertex(c)
				surface.add_uv(uva)
				surface.add_normal(na)
				surface.add_vertex(a)
				surface.add_uv(uvb)
				surface.add_normal(nb)
				surface.add_vertex(b)

			if c != d:
				surface.add_uv(uvc)
				surface.add_normal(nc)
				surface.add_vertex(c)
				surface.add_uv(uvb)
				surface.add_normal(nb)
				surface.add_vertex(b)
				surface.add_uv(uvd)
				surface.add_normal(nd)
				surface.add_vertex(d)
	
	surface.index()
	surface.generate_tangents()
	inst.mesh = surface.commit()
	inst.mesh.surface_set_material(0, material)
	add_child(inst)
	var collision = CollisionShape.new()
	collision.shape = inst.mesh.create_trimesh_shape()
	add_child(collision)
