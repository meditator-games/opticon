# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends Node

const _SETTINGS_FILE = "user://settings.json"
const _PROP_BLACKLIST = ["resource_name", "resource_local_to_scene", "script"]

var window_mode: int setget set_window_mode, get_window_mode
var msaa: int = 0 setget set_msaa
var ssao: int = 0 setget set_ssao
var reflections: int = 1 setget set_reflections
var sfx_volume: float = 1.0 setget set_sfx_volume, get_sfx_volume
var music_volume: float = 0.8 setget set_music_volume, get_music_volume

var _default_binds = {}

static func _is_bindable_action(action: String) -> bool:
	return not action.begins_with("ui_") and action != "click"

func _init():
	for action in InputMap.get_actions():
		if _is_bindable_action(action):
			_default_binds[action] = InputMap.get_action_list(action)
	set_sfx_volume(sfx_volume)
	set_music_volume(music_volume)
			
	var file = File.new()
	if not file.file_exists(_SETTINGS_FILE):
		return
	file.open(_SETTINGS_FILE, File.READ)
	if file.eof_reached():
		return
	var settings = parse_json(file.get_line())
	file.close()
	if settings.has("window_mode") and typeof(settings["window_mode"]) == TYPE_REAL:
		set_window_mode(int(settings["window_mode"]))
	if settings.has("msaa") and typeof(settings["msaa"]) == TYPE_REAL:
		set_msaa(int(settings["msaa"]))
	if settings.has("ssao") and typeof(settings["ssao"]) == TYPE_REAL:
		set_ssao(int(settings["ssao"]))
	if settings.has("reflections") and typeof(settings["reflections"]) == TYPE_REAL:
		reflections = int(settings["reflections"])
	if settings.has("sfx_volume") and typeof(settings["sfx_volume"]) == TYPE_REAL:
		set_sfx_volume(settings["sfx_volume"])
	if settings.has("music_volume") and typeof(settings["music_volume"]) == TYPE_REAL:
		set_music_volume(settings["music_volume"])
	if settings.has("binds") and typeof(settings["binds"]) == TYPE_DICTIONARY:
		for action in settings["binds"].keys():
			if _is_bindable_action(action):
				InputMap.action_erase_events(action)
				for bind in settings["binds"][action]:
					if bind.has("class") and typeof(bind["class"]) == TYPE_STRING and bind["class"].begins_with("InputEvent"):
						var event = _deserialize_object(bind)
						InputMap.action_add_event(action, event)

static func _deserialize_object(dict: Dictionary):
	var obj = ClassDB.instance(dict["class"])
	for prop in dict.keys():
		if prop != "class" and not _PROP_BLACKLIST.has(prop):
			obj.set(prop, dict[prop])
	return obj

static func _serialize_object(obj: Object):
	var dict = {"class": obj.get_class()}
	for prop in obj.get_property_list():
		var name = prop["name"]
		if prop["usage"] & PROPERTY_USAGE_STORAGE and not _PROP_BLACKLIST.has(name):
			dict[name] = obj.get(name)
	return dict
	
func reset_binds():
	for action in _default_binds.keys():
		InputMap.action_erase_events(action)
		for event in _default_binds[action]:
			InputMap.action_add_event(action, event)

func save():
	var settings = {
		"binds": {},
		"window_mode": get_window_mode(),
		"msaa": msaa,
		"ssao": ssao,
		"reflections": reflections,
		"sfx_volume": get_sfx_volume(),
		"music_volume": get_music_volume()
	}
	for action in InputMap.get_actions():
		if _is_bindable_action(action):
			var binds = []
			for event in InputMap.get_action_list(action):
				binds.append(_serialize_object(event))
			settings["binds"][action] = binds
	
	var file = File.new()
	file.open(_SETTINGS_FILE, File.WRITE)
	file.store_line(to_json(settings))
	file.close()

func get_window_mode():
	if OS.window_fullscreen:
		if OS.window_borderless:
			return 2
		return 1
	return 0

func set_window_mode(value):
	match value:
		0:
			OS.window_fullscreen = false
		1:
			OS.window_fullscreen = true
			OS.window_borderless = false
		2:
			OS.window_fullscreen = true
			OS.window_borderless = true
	
func set_msaa(value):
	msaa = value
	var viewport = Global.get_game_viewport()
	if viewport != null:
		viewport.msaa = value
	
func set_ssao(value):
	ssao = value
	var viewport = Global.get_game_viewport()
	if viewport == null:
		return
	var camera = viewport.get_camera()
	if camera == null:
		return
	if value == 0:
		camera.environment.ssao_enabled = false
	else:
		camera.environment.ssao_enabled = true
		camera.environment.ssao_quality = value - 1
	
func set_reflections(value):
	reflections = value
	var viewport = Global.get_game_viewport()
	if viewport == null:
		return
	var world_root = viewport.get_node("Root")
	world_root.resize_reflect_buffer(value)

enum AudioBusNames { MASTER, WORLD, MUSIC }

func get_sfx_volume():
	return pow(AudioServer.get_bus_volume_db(AudioBusNames.WORLD) / 80 + 1, 2.0)

func set_sfx_volume(value):
	sfx_volume = value
	AudioServer.set_bus_volume_db(AudioBusNames.WORLD, (pow(value, 1/2.0) - 1) * 80)
	
func get_music_volume():
	return pow(AudioServer.get_bus_volume_db(AudioBusNames.MUSIC) / 80 + 1, 2.0)
	
func set_music_volume(value):
	music_volume = value
	AudioServer.set_bus_volume_db(AudioBusNames.MUSIC, (pow(value, 1/2.0) - 1) * 80)
	
func update_current_viewport():
	set_msaa(msaa)
	set_ssao(ssao)