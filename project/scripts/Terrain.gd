# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends Spatial

class_name Terrain

const EdibleMushroom = preload("res://actors/EdibleMushroom.tscn")
const Mushroom = preload("res://actors/Mushroom.tscn")
const EvilEye = preload("res://actors/EvilEye.tscn")
const Orb = preload("res://actors/Orb.tscn")

const SCALE = 8.0
const CONTINENT_SCALE = 1.0/4.0
const Y_SCALE = SCALE * 32.0
const Y_SCALE_HILLS = SCALE * 32.0
const Y_SCALE_VALLEY = SCALE * 16.0
const Y_SCALE_CONTINENT = SCALE * 128.0
const CHUNKSIZE = 32.0
const REALCHUNKSIZE = CHUNKSIZE * SCALE
export (int, 0, 8) var view_chunks = 4

export (int) var collision_layer = 2
export (int) var collision_mask = 0
export (NodePath) var viewpoint_path = null
export (Material) var material = null

var load_progress = 0
var random = RandomNumberGenerator.new()

onready var _viewpoint: Spatial = get_node(viewpoint_path)
var _noise: TerrainNoise
var _active_chunks : Dictionary = {} # currenty loaded & visible
var _loading_chunks = []
var _visible_chunks = []
var _pending_chunks = []             # input to work thread
var _new_chunks = []                 # output from work thread
var _new_chunk_keys = []             # output keys from work thread
var _thread_quit: bool = false       # work thread exit flag
var _work_thread: Thread
var _chunk_mutex: Mutex = Mutex.new() # protects pending_chunks, new_chunks, thread_quit
var _work_count: Semaphore = Semaphore.new()
var _spawned_enemies = {}
var _spawned_orbs = {}

func start_work_thread():
	if _work_thread == null:
		_noise = TerrainNoise.new(random)
		_work_thread = Thread.new()
		process_worldgen()
		_work_thread.start(self, "_chunk_generate_loop")

func process_worldgen():
	var to_remove = _active_chunks.keys()
	var to_add
	
	_chunk_mutex.lock()
	_visible_chunks = _get_visible_chunks()
	for pos in _visible_chunks:
		if not _active_chunks.has(pos) and not _pending_chunks.has(pos) and not _new_chunk_keys.has(pos) and not _loading_chunks.has(pos):
			_pending_chunks.append(pos)
			_work_count.post()
		to_remove.erase(pos)
	to_add = _new_chunks.duplicate()
	_new_chunks = []
	_new_chunk_keys = []
	load_progress = 1.0 - _pending_chunks.size() / float(_visible_chunks.size())
	_chunk_mutex.unlock()
	
	for chunk in to_add:
		add_child(chunk)
		for f in chunk.objects:
			add_child(f)
		for f in chunk.actors:
			get_parent().add_child(f)
		_active_chunks[chunk.key] = chunk
		
	for pos in to_remove:
		var chunk = _active_chunks[pos]
		for o in chunk.objects:
			o.queue_free()
		remove_child(chunk)
		chunk.queue_free()
		_active_chunks.erase(pos)

func _chunk_generate_loop(obj):
	_chunk_mutex.lock()
	while not _thread_quit:
		_chunk_generate_step()
	_chunk_mutex.unlock()

func _chunk_generate_step(): # mutex must be locked upon calling this
	_chunk_mutex.unlock()
	_work_count.wait()
	_chunk_mutex.lock()
	if not _pending_chunks.empty():
		var pos: Vector2 = _pending_chunks.pop_front()
		if _visible_chunks.has(pos):
			_loading_chunks.append(pos)
			_chunk_mutex.unlock()
			var chunk_seed = int(_noise.objects.get_noise_2dv(pos) * 0x7fffffff)
			var chunk = Chunk.new(pos, int(CHUNKSIZE), chunk_seed, _noise, material, self)
			_chunk_mutex.lock()
			_new_chunk_keys.append(pos)
			_new_chunks.append(chunk)
			_loading_chunks.erase(pos)
	
func _ready():
	if _work_thread == null:
		process_worldgen()
		_chunk_mutex.lock()
		while _pending_chunks.size() > 0:
			_chunk_generate_step()
		_chunk_mutex.unlock()
		start_work_thread()
		
func _notification(what):
	if what == NOTIFICATION_PREDELETE:
		_chunk_mutex.lock()
		_thread_quit = true
		_work_count.post()
		_chunk_mutex.unlock()
	

class TerrainNoise:
	var continents = OpenSimplexNoise.new()
	var terrain = OpenSimplexNoise.new()
	var hills = OpenSimplexNoise.new()
	var valley = OpenSimplexNoise.new()
	var texture = OpenSimplexNoise.new()
	var objects = OpenSimplexNoise.new()
	var foliage = OpenSimplexNoise.new()
	func _init(random: RandomNumberGenerator):
		continents.seed = random.randi()
		continents.octaves = 1
		continents.period = 256.0
		continents.persistence = 2.0
		terrain.seed = random.randi()
		terrain.octaves = 4
		terrain.period = 256.0
		terrain.persistence = 0.9
		hills.seed = random.randi()
		hills.octaves = 2
		hills.period = 256.0
		hills.persistence = 0.4
		valley.seed = random.randi()
		valley.octaves = 1
		valley.period = 512.0
		valley.persistence = 0.2
		texture.seed = random.randi()
		texture.octaves = 1
		texture.period = 128.0
		texture.persistence = 0.3
		objects.seed = random.randi()
		objects.octaves = 1
		objects.period = 0.1
		objects.persistence = 0
		foliage.seed = random.randi()
		foliage.period = 64.0
		
	static func whitenoise(x, y=0, z=0, w=0):
	    return fmod((sin(12.9898 * x + 78.233 * y + 17.3392 * z + 47.743 * w) * 43758.5453), 1.0)
		
	func height_at(pos: Vector2) -> float:
		var x : float = pos.x
		var z : float = pos.y
		var height = terrain.get_noise_2d(x, z) * Y_SCALE + whitenoise(x, z, terrain.seed) * sqrt(abs(texture.get_noise_2d(x, z)))
		var m = hills.get_noise_2d(x, z)
		var n = valley.get_noise_2d(x, z)
		n = range_lerp(n, -1, 1, 1, 4)
		if m >= -0.5:
			var f = range_lerp(m, -0.5, 1, 0, 1)
			height += pow(f, n) * Y_SCALE_HILLS
		elif m < -0.5:
			var f = abs(range_lerp(m, -1, -0.5, -1, 0))
			height += pow(f, n) * Y_SCALE_VALLEY
		var c = continents.get_noise_2d(x * CONTINENT_SCALE, z * CONTINENT_SCALE)
		#if c < 0:
			#height += pow(c, 1.0/8.0) * Y_SCALE_CONTINENT
		height += c * Y_SCALE_CONTINENT
		return height
	
class Chunk extends StaticBody:
	var key: Vector2
	var noise: TerrainNoise
	var cache: Dictionary = {}
	var upper: int = 1000000
	var lower: int = -1000000
	var objects = []
	var actors = []
#	var _shape = PhysicsServer.shape_create(PhysicsServer.SHAPE_HEIGHTMAP)
#	var _body = PhysicsServer.body_create(PhysicsServer.BODY_MODE_STATIC)
	
	func _init(pos: Vector2, size: int, object_seed: int, noise: TerrainNoise, material: Material, parent: Spatial):
		._init()
#		PhysicsServer.body_add_shape(_body, _shape)
#		PhysicsServer.body_set_collision_layer(_body, 1)
#		PhysicsServer.body_set_collision_mask(_body, 1)
		self.noise = noise
		name = "Chunk" + str(pos)
		key = pos
		transform.origin = Vector3(pos.x * SCALE, 0, pos.y * SCALE)
		collision_layer = collision_layer
		collision_mask = collision_mask
		
		var rand = RandomNumberGenerator.new()
		rand.seed = object_seed
		rand.randi() # first result after setting seed is bad
		
		var upper = null
		var lower = null
		var heights = PoolRealArray()
		var surface = SurfaceTool.new()
		surface.begin(Mesh.PRIMITIVE_TRIANGLES)
		var shroom_count = 0
		var tallest = null
		for z in range(pos.y, pos.y + size):
			for x in range(pos.x, pos.x + size):
				var a = _vertex_at(Vector2(x, z))
				var b = _vertex_at(Vector2(x + 1, z))
				var c = _vertex_at(Vector2(x + 1, z + 1))
				var d = _vertex_at(Vector2(x, z + 1))
				heights.append(a.y)
				if upper == null:
					upper = a.y
					lower = a.y
				upper = max(max(a.y, c.y), upper)
				lower = min(min(a.y, c.y), lower)
				
				var na = _normal_at(a + transform.origin)
				var nb = _normal_at(b + transform.origin)
				var nc = _normal_at(c + transform.origin)
				var nd = _normal_at(d + transform.origin)
				
				surface.add_normal(na)
				surface.add_uv(Vector2((x - pos.x) / size, (z - pos.y) / size))
				surface.add_vertex(a)
				surface.add_normal(nb)
				surface.add_uv(Vector2((x - pos.x + 1) / size, (z - pos.y) / size))
				surface.add_vertex(b)
				surface.add_normal(nd)
				surface.add_uv(Vector2((x - pos.x) / size, (z - pos.y + 1) / size))
				surface.add_vertex(d)
				surface.add_normal(nd)
				surface.add_uv(Vector2((x - pos.x) / size, (z - pos.y + 1) / size))
				surface.add_vertex(d)
				surface.add_normal(nb)
				surface.add_uv(Vector2((x - pos.x + 1) / size, (z - pos.y) / size))
				surface.add_vertex(b)
				surface.add_normal(nc)
				surface.add_uv(Vector2((x - pos.x + 1) / size, (z - pos.y + 1) / size))
				surface.add_vertex(c)
				
				if int(x) % 4 == 0 and int(z) % 4 == 0:
					var m = noise.foliage.get_noise_2d(x, z)
					if m > 0.4:
						shroom_count += 1
						var mushroom = Mushroom.instance()
						mushroom.stem_rad = rand.rand_range(1.0, 2.0)
						mushroom.rad = mushroom.stem_rad * rand.rand_range(8.0, 16.0)
						mushroom.stem_rad_top = mushroom.stem_rad * rand.rand_range(0.7, 0.9)
						mushroom.height = pow(rand.rand_range(2.0, 8.0), 2.0)
						mushroom.cap_top_yscale = rand.rand_range(0.2, 0.8)
						mushroom.cap_bot_yscale = rand.rand_range(0.1, 0.6)
						mushroom.base_type = 'rounded'
						
						mushroom.stem_base_rad = mushroom.stem_rad * 2.0
						mushroom.stem_base_yscale = rand.rand_range(0.5, 2.0)
						
						mushroom.cap_bot_steps = max(2.0, floor(mushroom.rad * (1.0/16.0))) 
						mushroom.cap_top_steps = max(4.0, floor(mushroom.rad * (1.0/8.0)))
						mushroom.base_steps = mushroom.cap_bot_steps
						mushroom.lathe_steps = max(8.0, floor(mushroom.rad * (1.0/6.0)))
						mushroom.transform.origin = Vector3(x + rand.rand_range(-0.9, 0.9), 0, z + rand.rand_range(-0.9, 0.9)) * SCALE
						mushroom.transform.origin = get_heightmap(mushroom.transform.origin)
						mushroom.chunk = self
						objects.append(mushroom)
						if tallest == null or mushroom.height > tallest.height:
							tallest = mushroom
				elif int(x) % 2 == 0 and int(z) % 2 == 0:
					var m = noise.foliage.get_noise_2d(x, z)
					if m > 0.35 and randi() % 2 == 0:
						var mushroom = EdibleMushroom.instance()
						mushroom.stem_rad = rand.rand_range(0.05, 0.1)
						mushroom.rad = mushroom.stem_rad * rand.rand_range(8.0, 16.0)
						mushroom.stem_rad_top = mushroom.stem_rad * rand.rand_range(0.7, 0.9)
						mushroom.height = rand.rand_range(1.0, 3.0)
						mushroom.cap_top_yscale = rand.rand_range(0.2, 0.8)
						mushroom.cap_bot_yscale = rand.rand_range(0.1, 0.6)
						mushroom.base_type = 'rounded'
						
						mushroom.stem_base_rad = mushroom.stem_rad * 2.0
						mushroom.stem_base_yscale = rand.rand_range(0.5, 2.0)
						
						mushroom.cap_bot_steps = max(2.0, floor(mushroom.rad * (1.0/16.0))) 
						mushroom.cap_top_steps = max(4.0, floor(mushroom.rad * (1.0/8.0)))
						mushroom.base_steps = mushroom.cap_bot_steps
						mushroom.lathe_steps = max(8.0, floor(mushroom.rad * (1.0/6.0)))
						mushroom.transform.origin = Vector3(x + rand.rand_range(-0.4, 0.4), 0, z + rand.rand_range(-0.4, 0.4)) * SCALE
						mushroom.transform.origin = get_heightmap(mushroom.transform.origin)
						mushroom.chunk = self
						objects.append(mushroom)
						
		
		surface.index()
		surface.generate_tangents()
		var inst = MeshInstance.new()
		inst.mesh = surface.commit()
		inst.mesh.surface_set_material(0, material)
		var collision = CollisionShape.new()
		collision.shape = inst.mesh.create_trimesh_shape()
		add_child(inst)
		add_child(collision)
		
		if tallest != null and not parent._spawned_orbs.has(pos):
			parent._spawned_orbs[pos] = 0
			var orb = Orb.instance()
			orb.transform = tallest.transform
			orb.transform.origin.y += tallest.height + 12.0
			actors.append(orb)
		
		if not parent._spawned_enemies.has(pos):
			var eye_roll = rand.randf()
			if eye_roll > 0.5 and tallest != null:
				parent._spawned_enemies[pos] = 0
				var eye: RigidBody = EvilEye.instance()
				var eye_pos = transform.origin + Vector3(rand.rand_range(0.1, 0.9), 0, rand.rand_range(0.1, 0.9)) * size * SCALE
				eye.transform.origin = get_heightmap(eye_pos) + Vector3(0, eye.FLOAT_HEIGHT, 0)
				eye.transform.basis = eye.transform.basis.rotated(Vector3(0, 1, 0), rand.rand_range(0, deg2rad(360)))
				eye.terrain = parent
				actors.append(eye)

#		PhysicsServer.shape_set_data(_shape, {
#			"width": size,
#			"depth": size,
#			"heights": heights,
#			"cell_size": SCALE,
#			"min_height": lower,
#			"max_height": upper
#		})
#		PhysicsServer.body_set_shape_transform(_body, 0, Transform(Basis(),
#			Vector3(pos.x / SCALE, lower, pos.y / SCALE) + Vector3(size, upper - lower, size) * 0.5).scaled(Vector3(SCALE, 1, SCALE)))
#
#	func _enter_tree():
#		PhysicsServer.body_set_space(_body, get_viewport().world.get_space())
#
#	func _exit_tree():
#		PhysicsServer.body_set_space(_body, RID())
#
#	func _notification(what: int):
#		if what == NOTIFICATION_PREDELETE:
#			PhysicsServer.free_rid(_body)
#			PhysicsServer.free_rid(_shape)
	
	func get_heightmap(pos: Vector3) -> Vector3:
		var grid_pos = Vector2(pos.x / SCALE, pos.z / SCALE)
		if cache.has(grid_pos):
			return cache[grid_pos] + transform.origin
		return Vector3(pos.x, noise.height_at(grid_pos) + transform.origin.y, pos.z)
		
	func _vertex_at(pos: Vector2) -> Vector3:
		if not cache.has(pos):
			cache[pos] = Vector3(pos.x * SCALE, noise.height_at(pos), pos.y * SCALE) - transform.origin
		return cache[pos]
	
	func _normal_at(pos: Vector3):
		var a = get_heightmap(pos + Vector3(SCALE, 0, 0))
		var b = get_heightmap(pos + Vector3(0, 0, SCALE))
		var c = get_heightmap(pos + Vector3(-SCALE, 0, 0))
		var d = get_heightmap(pos + Vector3(0, 0, -SCALE))
		var cab = (a - pos).cross(b - pos)
		var ccd = (c - pos).cross(d - pos)
		return -(cab + ccd).normalized()

static func _to_chunkpos(wpos: Vector3) -> Vector2:
	var cpos = wpos / SCALE
	return Vector2(stepify(cpos.x, CHUNKSIZE), stepify(cpos.z, CHUNKSIZE))
	
func get_heightmap(pos: Vector3) -> Vector3:
	var cpos = _to_chunkpos(pos)
	if _active_chunks.has(cpos):
		return _active_chunks[cpos].get_heightmap(pos)
	var grid_pos = Vector2(pos.x / SCALE, pos.z / SCALE)
	return Vector3(pos.x, _noise.height_at(grid_pos), pos.z)

class ChunkSorter:
	var camera_pos: Vector2
	func _init(camera_pos: Vector3):
		self.camera_pos = Vector2(camera_pos.x, camera_pos.z)
	func sort(a, b):
		return a.distance_squared_to(camera_pos) < b.distance_squared_to(camera_pos)

func _get_visible_chunks():
	var cam_pos: Vector3
	if _viewpoint == null or not _viewpoint.is_inside_tree():
		cam_pos = Vector3(0, 0, 0)
	else:
		cam_pos = _viewpoint.get_global_transform().origin / SCALE
	var sta = cam_pos - Vector3(CHUNKSIZE, 0, CHUNKSIZE) * view_chunks
	var end = cam_pos + Vector3(CHUNKSIZE, 0, CHUNKSIZE) * (view_chunks + 1.0)
	
	var active = []
	for z in range(sta.z, end.z, CHUNKSIZE):
		for x in range(sta.x, end.x, CHUNKSIZE):
			var pos = Vector2(stepify(x, CHUNKSIZE), stepify(z, CHUNKSIZE))
			active.append(pos)
	var sorter = ChunkSorter.new(cam_pos)
	active.sort_custom(sorter, "sort")
	return active
	
func _process(delta):
	if is_inside_tree():
		process_worldgen()