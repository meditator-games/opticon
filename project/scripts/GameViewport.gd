# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends ViewportContainer

export (Material) var underwater_material
export (Environment) var underwater_environment
export (Color) var underwater_color = Color("#9acbf1")
onready var invert_material = material

var last_underwater = false
	
func _ready():
	Settings.update_current_viewport()
	material.set_shader_param("invert", 0)

func _process(delta):
	var underwater = $Viewport/Root/Player.underwater
	if last_underwater != underwater:
		if underwater:
			$Viewport/Root/Player/Camera.environment = underwater_environment
			material = underwater_material
		else:
			$Viewport/Root/Player/Camera.environment = $Viewport.world.environment
			material = invert_material
		Settings.update_current_viewport()
		last_underwater = $Viewport/Root/Player.underwater
	if $Viewport/Root/Player.hp == 0:
		material.set_shader_param("invert", $Viewport/Root/Player.death_timer)
	else:
		material.set_shader_param("invert", 0)
	
func _input(event):
	if event is InputEventMouse:
		_gui_input(event)
		
func _gui_input(event):
	if event is InputEventMouse:
		var mouseEvent = event.duplicate()
		mouseEvent.position = get_global_transform().xform_inv(event.global_position)
		$Viewport.unhandled_input(mouseEvent)
	else:
		$Viewport.unhandled_input(event)
		