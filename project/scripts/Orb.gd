# This file is part of Opticon.
#
# Opticon is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Opticon is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Opticon.  If not, see <https://www.gnu.org/licenses/>.

extends MeshInstance

onready var init_pos = transform.origin
var float_timer = 0

func _physics_process(delta):
	transform.origin.y = init_pos.y + sin(float_timer)
	float_timer += delta
	rotation.y += delta 

func _on_Area_body_entered(body):
	if body.has_method("collect"):
		body.collect(self)
